
package com.ValdenirBrito.FormulaOne.Calculos;

import java.util.ArrayList;
import javax.swing.JTextPane;

/**
 *
 * @author v103760 Esta classe é reponsável por verificar a melhor volta da corrida
 * A lista é convertidada em um array que faz a formatação em colunas
 * separados por ; Exemplo o array[0] reprenta a coluna hora e assim por diante
 *
 */
public class MelhorVoltaCorrida {
    
     private static double valorInicial;
    
     public static void melhorVoltaCorrida(ArrayList lista, JTextPane painel) {

        valorInicial = 0;
        String valorFormato = null;
        String piloto = null;
        double valor1 = 0;
        double valor2 = 0;
       
       
        for (int i = 0; i < lista.size(); i++) {

            String array[] = new String[3];

            array = lista.get(i).toString().split(";");

            
       
            if(i ==0){
                valorInicial = Double.parseDouble(array[5].replaceAll(":", ""));
            }else{
              
              valor2 = Double.parseDouble(array[5].replaceAll(":", ""));  // Faz um raplace para facilitar a conversão em double
       
            if (valorInicial < valor2) {
                valor1 = valorInicial;
            } else {
                valorInicial = valor2;
                valorFormato = array[5]; //Pega o valor original
                piloto = array[3];      //pega o nome do piloto
            }
                
                
            }
            
          

        }
        System.out.println("Melhor volta da corrida "+ "piloto " + piloto + " Tempo "+ valorFormato);
        painel.setText(painel.getText() + "\n" + "Melhor volta da corrida "+ "piloto " + piloto + " Tempo "+ valorFormato);
    }
    
   
    
}
