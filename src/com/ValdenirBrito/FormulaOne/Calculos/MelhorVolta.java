package com.ValdenirBrito.FormulaOne.Calculos;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import javax.swing.JTextPane;

/**
 *
 * @author v103760 Esta classe é reponsável por verificar a melhor volta de cada
 * piloto A lista é convertidada em um array que faz a formatação em colunas
 * separados por ; Exemplo o array[0] reprenta a coluna hora e assim por diante
 *
 */
public class MelhorVolta {

    static NumberFormat formatter = new DecimalFormat("#0.000");

    public static void melhorVolta(String codigo, ArrayList lista, JTextPane painel) {

        double valor1 = 0;
        double valor2 = 0;

        String nome = null;
        int contador = 0;
        for (int i = 0; i < lista.size(); i++) {

            String array[] = new String[3];

            array = lista.get(i).toString().split(";");

            valor2 = Double.parseDouble(array[5].replaceAll(":", ""));

            if (array[1].equals(codigo)) {

                if (contador == 0) {
                    valor1 = Double.parseDouble(array[5].replaceAll(":", ""));
                    //   System.out.println("Primeiro " +valor1);
                } else {

                    nome = array[3];
                    if (valor1 > valor2) {
                        //     System.out.println("sim " +valor1 + " volta mais rapida " + valor2);
                        valor1 = valor2;
                    } else {

                    }
                }
                contador++;
            }

        }
        String total = "" + formatter.format(valor1);
        painel.setText(painel.getText() + "\n" + "Piloto  " + nome + " sua volta mais rapida foi: " + total.substring(0, total.length() - 6) + ":" + total.substring(total.length() - 6).replaceAll(",", ".")+" minutos");

    }
}
