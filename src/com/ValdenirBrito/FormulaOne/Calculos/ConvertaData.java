/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ValdenirBrito.FormulaOne.Calculos;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author v103760
 */
public class ConvertaData {
    
    
    public static String converteData(Double valor){
         double x = valor;
       String s = String.format("%06d", (int) x);
       DateFormat format = new SimpleDateFormat("HHmmss");
       try {
           Date date = format.parse(s);
           System.out.println("Data" + date.getMinutes() + ":" + date.getSeconds());
           String data = date.getMinutes() + ":" + date.getSeconds() +"."+ date.getTime();
            return data;
            } catch (ParseException ex) {
                      Logger.getLogger(TempoTotal.class.getName()).log(Level.SEVERE, null, ex);
                  }  
            return null;
    }
    
}
