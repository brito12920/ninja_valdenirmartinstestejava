
package com.ValdenirBrito.FormulaOne.Calculos;

import java.util.ArrayList;
import javax.swing.JTextPane;

/**
 *
 * @author v103760
 * Esta classe é reponsavel por calcular as diferente entro o vencedor e o piloto e o tempo total de cada piloto
 */
public class TempoTotal {

    private static double vencedor;
    private static String pilotoVencedor;

    public static void TempoTotal(String codigo, ArrayList lista, JTextPane painel, int contadorGeral) {
       
        double valor = 0;
       
        String piloto = null;

        for (int i = 0; i < lista.size(); i++) {

            String array[] = new String[3];

            array = lista.get(i).toString().split(";");

            if (array[1].equals(codigo)) {

                valor += Double.parseDouble(array[5].replaceAll(":", ""));
                piloto = array[3];

            }

        }

        if (contadorGeral == 0) {
            vencedor = valor;
            pilotoVencedor = piloto;
        } else if (vencedor > valor) {

        } else {
            double diferenca = valor - vencedor;

           //  System.out.println("total"+valor + diferenca);
            
            painel.setText(painel.getText() + "\n" + "o Piloto " + piloto + " chegou com a diferença de  " + ConvertaData.converteData(diferenca) + " depois do vencedor " + pilotoVencedor);
        }

       

        painel.setText(painel.getText() + "\n" + "O piloto " + piloto + " teve um tempo total de: " + ConvertaData.converteData(valor));

        painel.setText(painel.getText() + "\n" + "O vencedor foi " + pilotoVencedor + " com o tempo de " + ConvertaData.converteData(vencedor));
        painel.setText(painel.getText() + "\n" + "--------------------------------------------------------------------------------------------------------------------------------------------------");

    }
}
